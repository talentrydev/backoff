<?php

declare(strict_types=1);

namespace Talentry\Backoff\Tests;

use InvalidArgumentException;
use JsonSerializable;
use PHPUnit\Framework\TestCase;
use RuntimeException;
use SplStack;
use Talentry\Backoff\BackoffDecorator;

class BackoffDecoratorTest extends TestCase
{
    public function testDecorate(): void
    {
        $returnValue = 'foo';
        //object will throw an exception twice and then will return a valid value
        $malfunctioningObject = $this->getMalfunctioningObject(2, $returnValue);
        //backoff is configured to retry 3 times; therefore we should get the correct response
        $decoratedObject = BackoffDecorator::maxAttempts(3)->decorate($malfunctioningObject);
        self::assertSame($returnValue, $decoratedObject->jsonSerialize());

        //backoff is configured to retry 2 times; therefore we should not get the correct response
        $decoratedObject = BackoffDecorator::maxAttempts(2)->decorate($malfunctioningObject);
        try {
            $decoratedObject->jsonSerialize();
            self::fail();
        } catch (RuntimeException) {
        }
    }

    public function testInvokeNonExistentMethod(): void
    {
        $delegate = new SplStack();
        $decoratedObject = BackoffDecorator::maxAttempts(3)->decorate($delegate);
        try {
            $decoratedObject->foo();
            self::fail();
        } catch (InvalidArgumentException $e) {
            self::assertSame('Method SplStack::foo does not exist', $e->getMessage());
        }
    }

    private function getMalfunctioningObject(int $maxFailures, string $returnValue): object
    {
        return new class ($maxFailures, $returnValue) implements JsonSerializable {
            public function __construct(
                private readonly int $maxFailures,
                private readonly string $returnValue,
                private int $failureCount = 0,
            ) {
            }

            public function jsonSerialize(): mixed
            {
                if ($this->failureCount >= $this->maxFailures) {
                    return $this->returnValue;
                }

                $this->failureCount++;

                throw new RuntimeException('Simulated failure');
            }
        };
    }
}
