<?php

declare(strict_types=1);

namespace Talentry\Backoff\WaitStrategies;

class USleepStrategy implements WaitStrategy
{
    public function wait(int $milliseconds): void
    {
        usleep(
            // usleep sleeps for x MICROseconds so we have to multiply by 1000
            $milliseconds * 1_000,
        );
    }
}
