<?php

declare(strict_types=1);

namespace Talentry\Backoff;

use Talentry\Backoff\BackoffStrategies\BackoffStrategy;
use Talentry\Backoff\BackoffStrategies\ExponentialStrategy;
use Talentry\Backoff\JitterStrategies\FullJitterStrategy;
use Talentry\Backoff\JitterStrategies\JitterStrategy;
use Talentry\Backoff\RetryDeciderStrategies\MaxAttemptStrategy;
use Talentry\Backoff\RetryDeciderStrategies\RetryDeciderStrategy;
use Talentry\Backoff\WaitStrategies\USleepStrategy;
use Talentry\Backoff\WaitStrategies\WaitStrategy;
use Throwable;

class Backoff
{
    private BackoffStrategy $backoffStrategy;
    private WaitStrategy $waitStrategy;
    private RetryDeciderStrategy $retryDeciderStrategy;
    private JitterStrategy $jitterStrategy;

    public function __construct()
    {
        $this->backoffStrategy = new ExponentialStrategy(200);
        $this->waitStrategy = new USleepStrategy();
        $this->retryDeciderStrategy = new MaxAttemptStrategy(4);
        $this->jitterStrategy = new FullJitterStrategy();
    }

    public function run(callable $function)
    {
        $currentAttempt = 1;

        while (true) {
            $callableResult = null;

            try {
                $callableResult = $function();

                return $callableResult;
            } catch (Throwable $exception) {
                $shouldRetry = $this->retryDeciderStrategy->shouldRetry(
                    $currentAttempt,
                    $exception,
                    $callableResult
                );

                if (!$shouldRetry) {
                    throw $exception;
                }

                $waitTime = $this->backoffStrategy->getWaitTime($currentAttempt);

                $jitteredWaitTime = $this->jitterStrategy->jitter($waitTime);

                $this->waitStrategy->wait($jitteredWaitTime);

                $currentAttempt++;
            }
        }

        return $function();
    }

    public function setBackoffStrategy(BackoffStrategy $backoffStrategy): self
    {
        $this->backoffStrategy = $backoffStrategy;

        return $this;
    }

    public function setWaitStrategy(WaitStrategy $waitStrategy): self
    {
        $this->waitStrategy = $waitStrategy;

        return $this;
    }

    public function setRetryDeciderStrategy(RetryDeciderStrategy $retryDeciderStrategy): self
    {
        $this->retryDeciderStrategy = $retryDeciderStrategy;

        return $this;
    }

    public function setJitterStrategy(JitterStrategy $jitterStrategy): self
    {
        $this->jitterStrategy = $jitterStrategy;

        return $this;
    }
}
