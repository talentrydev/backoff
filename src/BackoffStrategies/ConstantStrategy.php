<?php

declare(strict_types=1);

namespace Talentry\Backoff\BackoffStrategies;

class ConstantStrategy extends AbstractStrategy
{
    public function getWaitTime(int $currentAttempt): int
    {
        return $this->baseWaitTime;
    }
}
