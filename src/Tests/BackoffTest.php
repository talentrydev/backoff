<?php

declare(strict_types=1);

namespace Talentry\Backoff\Tests;

use PHPUnit\Framework\TestCase;
use RangeException;
use RuntimeException;
use Talentry\Backoff\Backoff;
use Talentry\Backoff\RetryDeciderStrategies\MaxAttemptStrategy;
use Talentry\Backoff\RetryDeciderStrategies\RetryDeciderStrategy;
use Talentry\Backoff\WaitStrategies\VoidStrategy;
use Throwable;

class BackoffTest extends TestCase
{
    public function testMaxAttempts(): void
    {
        $calls = 0;
        $maxAttempts = 4;

        $fn = function () use (&$calls) {
            $calls++;

            throw new RuntimeException();
        };

        try {
            (new Backoff())
                ->setRetryDeciderStrategy(new MaxAttemptStrategy($maxAttempts))
                ->setWaitStrategy(new VoidStrategy()) // avoid waiting
                ->run($fn);

            // this should never be called. If it is, something is fishy
            self::assertSame(false, true);
        } catch (RuntimeException) {
            // void
        }

        self::assertSame($maxAttempts, $calls);
    }

    public function testWithCustomRetryDeciderStrategy(): void
    {
        $calls = 0;

        $fn = function () use (&$calls) {
            $calls++;

            throw new RangeException();
        };

        try {
            $customRetryStrategy = new class implements RetryDeciderStrategy {
                public function shouldRetry(
                    int $currentAttempt,
                    Throwable $exception,
                    mixed $callableResult = null,
                ): bool {
                    // if we receive a RangeException stop trying immediately
                    return !$exception instanceof RangeException;
                }
            };

            (new Backoff())
                ->setRetryDeciderStrategy($customRetryStrategy)
                ->setWaitStrategy(new VoidStrategy()) // avoid waiting
                ->run($fn);

            // this should never be called. If it is, something is fishy
            self::assertSame(false, true);
        } catch (Throwable) {
            // void
        }

        self::assertSame(1, $calls);
    }
}
