<?php

declare(strict_types=1);

namespace Talentry\Backoff\JitterStrategies;

interface JitterStrategy
{
    public function jitter(int $waitTime): int;
}
