<?php

declare(strict_types=1);

namespace Talentry\Backoff\WaitStrategies;

interface WaitStrategy
{
    public function wait(int $milliseconds): void;
}
