<?php

declare(strict_types=1);

namespace Talentry\Backoff\JitterStrategies;

class FullJitterStrategy implements JitterStrategy
{
    public function jitter(int $waitTime): int
    {
        return random_int(0, $waitTime);
    }
}
