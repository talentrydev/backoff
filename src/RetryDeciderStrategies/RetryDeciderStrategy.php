<?php

declare(strict_types=1);

namespace Talentry\Backoff\RetryDeciderStrategies;

use Throwable;

interface RetryDeciderStrategy
{
    /**
     * @param mixed $callableResult This is the result of the function you pass in
     */
    public function shouldRetry(int $currentAttempt, Throwable $exception, mixed $callableResult = null): bool;
}
