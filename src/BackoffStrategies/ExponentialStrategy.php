<?php

declare(strict_types=1);

namespace Talentry\Backoff\BackoffStrategies;

class ExponentialStrategy extends AbstractStrategy
{
    public function getWaitTime(int $currentAttempt): int
    {
        // we subtract 1 so that on the first try, we actually wait for the $baseWaitTime
        // 2**0 = 1
        return (2 ** ($currentAttempt - 1)) * $this->baseWaitTime;
    }
}
