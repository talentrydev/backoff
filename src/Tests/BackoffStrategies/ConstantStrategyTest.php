<?php

declare(strict_types=1);

namespace Talentry\Backoff\Tests\BackoffStrategies;

use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;
use Talentry\Backoff\BackoffStrategies\ConstantStrategy;

class ConstantStrategyTest extends TestCase
{
    #[DataProvider('dataProvider')]
    public function testStrategy(int $attempt, int $baseWaitTime, int $expected): void
    {
        $exponentialStrategy = new ConstantStrategy($baseWaitTime);

        self::assertSame(
            $expected,
            $exponentialStrategy->getWaitTime($attempt),
        );
    }

    public static function dataProvider(): array
    {
        return [
            [1, 200, 200],
            [2, 200, 200],
            [3, 200, 200],
            [4, 200, 200],
            [5, 200, 200],
            [5, 0, 0],
        ];
    }
}
