<?php

declare(strict_types=1);

namespace Talentry\Backoff\BackoffStrategies;

abstract class AbstractStrategy implements BackoffStrategy
{
    public function __construct(
        protected readonly int $baseWaitTime,
    ) {
    }
}
