<?php

declare(strict_types=1);

namespace Talentry\Backoff;

use InvalidArgumentException;
use Talentry\Backoff\RetryDeciderStrategies\MaxAttemptStrategy;

class BackoffDecorator
{
    public function __construct(
        private readonly Backoff $backoff = new Backoff(),
    ) {
    }

    /**
     * @template T of object
     * @param T $object
     * @return T
     */
    public function decorate(object $object): object
    {
        /** @var T $class */
        $class = new class ($object, $this->backoff) {
            public function __construct(
                private readonly object $delegate,
                private readonly Backoff $backoff,
            ) {
            }

            /** @phpstan-ignore-next-line  */
            public function __call(string $name, array $arguments): mixed
            {
                if (!method_exists($this->delegate, $name)) {
                    throw new InvalidArgumentException(sprintf(
                        'Method %s::%s does not exist',
                        $this->delegate::class,
                        $name,
                    ));
                }

                return $this->backoff->run(function () use ($name, $arguments) {
                    return call_user_func_array([$this->delegate, $name], $arguments); //@phpstan-ignore-line
                });
            }
        };

        return $class;
    }

    public static function maxAttempts(int $maxAttempts): self
    {
        return new self((new Backoff())->setRetryDeciderStrategy(new MaxAttemptStrategy($maxAttempts)));
    }
}
