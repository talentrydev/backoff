<?php

declare(strict_types=1);

namespace Talentry\Backoff\JitterStrategies;

class NoJitterStrategy implements JitterStrategy
{
    public function jitter(int $waitTime): int
    {
        return $waitTime;
    }
}
