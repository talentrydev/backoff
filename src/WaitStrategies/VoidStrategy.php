<?php

declare(strict_types=1);

namespace Talentry\Backoff\WaitStrategies;

class VoidStrategy implements WaitStrategy
{
    public function wait(int $milliseconds): void
    {
        // do nothing
    }
}
