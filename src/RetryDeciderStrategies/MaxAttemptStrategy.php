<?php

declare(strict_types=1);

namespace Talentry\Backoff\RetryDeciderStrategies;

use Throwable;

class MaxAttemptStrategy implements RetryDeciderStrategy
{
    public function __construct(
        private readonly int $maxAttempts = 4,
    ) {
    }

    public function shouldRetry(int $currentAttempt, Throwable $exception, mixed $callableResult = null): bool
    {
        return $currentAttempt < $this->maxAttempts;
    }
}
