<?php

declare(strict_types=1);

namespace Talentry\Backoff\RetryDeciderStrategies;

use Throwable;

class CompositeStrategy implements RetryDeciderStrategy
{
    /**
     * @var RetryDeciderStrategy[]
     */
    private array $strategies = [];

    public function addStrategy(RetryDeciderStrategy $retryDeciderStrategy): self
    {
        $this->strategies[] = $retryDeciderStrategy;

        return $this;
    }

    public function shouldRetry(int $currentAttempt, Throwable $exception, mixed $callableResult = null): bool
    {
        foreach ($this->strategies as $strategy) {
            if (!$strategy->shouldRetry($currentAttempt, $exception, $callableResult)) {
                return false;
            }
        }

        return true;
    }
}
