<?php

declare(strict_types=1);

namespace Talentry\Backoff\Tests\RetryDeciderStrategies;

use PHPUnit\Framework\TestCase;
use RuntimeException;
use Talentry\Backoff\RetryDeciderStrategies\CompositeStrategy;
use Talentry\Backoff\RetryDeciderStrategies\MaxAttemptStrategy;
use Talentry\Backoff\RetryDeciderStrategies\RetryDeciderStrategy;
use Throwable;

class CompositeStrategyTest extends TestCase
{
    public function testStrategy(): void
    {
        $strategy = new CompositeStrategy();

        $voidStrategy = new class implements RetryDeciderStrategy {
            public function shouldRetry(int $currentAttempt, Throwable $exception, mixed $callableResult = null): bool
            {
                return true;
            }
        };

        $strategy
            ->addStrategy($voidStrategy)
            ->addStrategy(new MaxAttemptStrategy(4));

        // the void strategy will always say to retry so the MaxAttemptStrategy should kick in after
        // the 4th retry
        self::assertFalse(
            $strategy->shouldRetry(4, new RuntimeException()),
        );

        self::assertFalse(
            $strategy->shouldRetry(5, new RuntimeException()),
        );
    }
}
