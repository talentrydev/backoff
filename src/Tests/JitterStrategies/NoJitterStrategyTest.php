<?php

declare(strict_types=1);

namespace Talentry\Backoff\Tests\JitterStrategies;

use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;
use Talentry\Backoff\JitterStrategies\NoJitterStrategy;

class NoJitterStrategyTest extends TestCase
{
    #[DataProvider('dataProvider')]
    public function testStrategy(int $waitTime, int $expectedWaitTime): void
    {
        $strategy = new NoJitterStrategy();

        self::assertSame($expectedWaitTime, $strategy->jitter($waitTime));
    }

    public static function dataProvider(): array
    {
        return [
            [1, 1],
            [2, 2],
            [3, 3],
        ];
    }
}
