<?php

declare(strict_types=1);

namespace Talentry\Backoff\BackoffStrategies;

interface BackoffStrategy
{
    public function getWaitTime(int $currentAttempt): int;
}
