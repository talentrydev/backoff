<?php

declare(strict_types=1);

namespace Talentry\Backoff\Tests\RetryDeciderStrategies;

use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;
use RuntimeException;
use Talentry\Backoff\RetryDeciderStrategies\MaxAttemptStrategy;

class MaxAttemptStrategyTest extends TestCase
{
    #[DataProvider('dataProvider')]
    public function testStrategy($currentAttempt, $maxAttempts, $expectedResult)
    {
        self::assertSame(
            $expectedResult,
            (new MaxAttemptStrategy($maxAttempts))->shouldRetry($currentAttempt, new RuntimeException()),
        );
    }

    public static function dataProvider(): array
    {
        return [
            [1, 3, true],
            [2, 3, true],
            [3, 3, false],
            [4, 3, false],
        ];
    }
}
