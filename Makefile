PHP_IMAGE=php:8.3-cli

deps:
	docker run --rm -t -v $$(pwd):/app -w /app composer install
test:
	docker run --rm -t -v $$(pwd):/app -w /app $(PHP_IMAGE) ./vendor/bin/phpunit
cs:
	docker run --rm -t -v $$(pwd):/app -w /app $(PHP_IMAGE) ./vendor/bin/phpcs --standard=PSR12 src
csfix:
	docker run --rm -t -v $$(pwd):/app -w /app $(PHP_IMAGE) ./vendor/bin/phpcbf --standard=PSR12 src
