<?php

declare(strict_types=1);

namespace Talentry\Backoff\Tests\BackoffStrategies;

use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;
use Talentry\Backoff\BackoffStrategies\ExponentialStrategy;

class ExponentialStrategyTest extends TestCase
{
    #[DataProvider('dataProvider')]
    public function testStrategy(int $attempt, int $baseWaitTime, int $expected)
    {
        $exponentialStrategy = new ExponentialStrategy($baseWaitTime);

        self::assertSame(
            $expected,
            $exponentialStrategy->getWaitTime($attempt),
        );
    }

    public static function dataProvider(): array
    {
        return [
            [1, 200, 200],
            [2, 200, 400],
            [3, 200, 800],
            [4, 200, 1_600],
            [5, 200, 3_200],
            [5, 0, 0],
        ];
    }
}
